/*
 * Dog.h
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef DOG_H_
#define DOG_H_

#include "Animal.h"

class Dog: public Animal {
public:
//	using Animal::speak; //using methods of base class
	Dog(std::string givenName);
//	void speak(); // shadows Animal speak
private:
	void giveASound();
};

#endif /* DOG_H_ */
