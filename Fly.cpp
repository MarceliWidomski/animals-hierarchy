/*
 * Fly.cpp
 *
 *  Created on: 29.03.2017
 *      Author: marce
 */

#include <iostream>
#include "Fly.h"

Fly::Fly(std::string givenName) :
		Insect(givenName) {
}
void Fly::giveASound (){
	std::cout<< "Humm!" << std::endl;
}
;

