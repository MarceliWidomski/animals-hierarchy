/*
 * Lion.h
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef LION_H_
#define LION_H_
#include "Animal.h"

class Lion: public Animal {
public:
	Lion(std::string givenName);
//	void speak();
private:
	void giveASound();
};

#endif /* LION_H_ */
