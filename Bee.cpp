/*
 * Bee.cpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#include "Bee.h"
#include <iostream>

Bee::Bee(std::string givenName): Insect (givenName) {}
void Bee::giveASound() {
	std::cout << "Buzz!" << std::endl;
}
