/*
 * Insect.h
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef INSECT_H_
#define INSECT_H_

#include "Animal.h"

class Insect: public Animal {
public:
Insect (std::string givenName);

private:
virtual void giveASound()=0;
};

#endif /* INSECT_H_ */
