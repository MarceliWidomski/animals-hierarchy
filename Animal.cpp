/*
 * Animal.cpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#include <iostream>
#include "Animal.h"

//Animal::Animal() {}
Animal::Animal(std::string givenName): name(givenName){
}

void Animal::speak(){
	std::cout << name << " speaks: ";
	giveASound();
	std::cout << std::endl;
}
std::string Animal::getMyName(){return std::string ("My name is ") + name;}
//void Animal::speak(int a){
//	std::cout << "Animal speaks, argument: " << a << std::endl;
//
//}
//void Animal::speak (char first, char second){
//	std::cout << "Animal speaks, arguments: " << first << " "<< second << std::endl;
//
//}
//void Animal::giveASound(){}
