/*
 * Fly.h
 *
 *  Created on: 29.03.2017
 *      Author: marce
 */

#ifndef FLY_H_
#define FLY_H_

#include <string>
#include "Insect.h"

class Fly: public Insect {
public:
	Fly(std::string givenName);
private:
	void giveASound();
};

#endif /* FLY_H_ */
