/*
 * Bee.h
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef BEE_H_
#define BEE_H_

#include "Animal.h"
#include "Insect.h"

class Bee: public Insect {
public:
	Bee(std::string givenName);
private:
	void giveASound();
};

#endif /* BEE_H_ */
