/*
 * Animal.h
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef ANIMAL_H_
#define ANIMAL_H_

#include <string>

class Animal {
public:
	Animal(std::string givenName);
	void speak();
	void speak(int a);
	void speak (char first, char second);
	std::string getMyName();

	const std::string& getName() const {return name;}
	void setName(const std::string& name) {this->name = name;}

private:
	virtual void giveASound()=0;//because method is pure virtual class animal is abstract //if we want to override a base class method it has to be virtual method!
	std::string name;
};

#endif /* ANIMAL_H_ */
