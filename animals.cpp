//============================================================================
// Name        : animals.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Animal.h"
#include "Dog.h"
#include "Lion.h"
#include "Insect.h"
#include "Bee.h"
#include "Fly.h"

using namespace std;

int main() {

	Dog maks("Maks");
	Lion rafael("Rafael"); //function speak isn't virtual, funtion giveASound is virtual
//	Animal thing ("Thing");
	cout << maks.getMyName() << endl; // if function is defined in base class we can use it for object of derivered class
	maks.speak();
	cout << rafael.getMyName() << endl;
	rafael.speak();
	Bee maya("Maya");
	cout << maya.getMyName()<<endl;
	maya.speak();
	Fly fly ("Fly");
	cout << fly.getMyName() << endl;
	fly.speak();






					//	Dog york; //using methods of base class
					//	york.speak();
					//	york.speak(3);
					//	york.speak('K','J');




		//	const int numberOfAnimals(5);

		//	Lion mufasa;
		//	Lion simba;
		//	Dog rhodesian;
		//	Dog boxer;
		//	Animal someAnimal;
		//
		//	Animal* animals[numberOfAnimals] = {&mufasa, &simba, &rhodesian, &boxer, &someAnimal}; //array of pointers, &mufasa - adress of mufasa
		//																							// array of pointers, important!!!!!
		//	for (int i = 0; i < numberOfAnimals; ++i) {
		//		animals[i] -> speak();
		//	}

		//	mufasa.Animal::speak(); //method speak from Animal class

//	Animal anAnimal;
//	Animal *someAnimal; //pointer at object of Animal class
//	Animal *otherAnimal;
//	anAnimal.speak();
//
//	Dog reks;
//	reks.speak();
//
//	Lion king;
//	king.speak();
//
//	someAnimal = &reks; //setting pointer at object of Dog class
//	someAnimal->speak(); // with pointer we can use only what is in Animal class
//
//	otherAnimal = &king;
//	otherAnimal->speak(); // because speak() is virtual we can use speak for lion using the pointer

	return 0;
}
